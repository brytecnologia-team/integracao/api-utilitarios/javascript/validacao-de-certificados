const { URL_SERVER_VALIDATOR, verify, configuredToken } = require('../functions/Validator');


const validatorReport = (result) => {

    console.log('Validator JSON response: ', result);

    var chainStatus = result.reports[0].status;
    const indexOfCertificate = chainStatus.certificateStatusList.length - 1;
    const certificateOfHolder = chainStatus.certificateStatusList[indexOfCertificate];

    console.log('Holder information: ')
    if (chainStatus != undefined && certificateOfHolder != undefined) {
        console.log('Name: ', certificateOfHolder.certificateInfo.subjectDN.cn);
        console.log('General status of the certificate chain: ', chainStatus.status);
        console.log('Certificate status: ', certificateOfHolder.status);
        if (certificateOfHolder.status === 'INVALID') {
            console.log(certificateOfHolder.errorStatus);
            if (certificateOfHolder.revocationStatus != undefined) {
                console.log(certificateOfHolder.revocationStatus);
            }
        }
        console.log('ICP-Brazil certificate: ', certificateOfHolder.pkiBrazil);
        console.log('Initial validity date of certificate: ', certificateOfHolder.certificateInfo.validity.notBefore);
        console.log('End date of validity of certificate: ', certificateOfHolder.certificateInfo.validity.notAfter);

    } else {
        console.log('Incomplete chain of the holder: The certificate could not be verified');
        console.log('General status of chain: ', chainStatus.status);
    }


}


async function main() {
    if (configuredToken()) {

        console.log('================Starting certificate verification ... ================');

        verify(URL_SERVER_VALIDATOR).then(result => {

            validatorReport(result);

        }).catch(error => {
            console.log(error);
        })


    }
}

main();