const request = require('request');
const serviceConfig = require('../config/ServiceConfig');
const certificateConfig = require('../config/CertificateConfig');

var fs = require('fs');

const URL_SERVER_VALIDATOR = serviceConfig.URL_VALIDATOR;

this.headers = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${serviceConfig.ACCESS_TOKEN}`
};

var auxAuthorization = this.headers.Authorization.split(' ');

const configuredToken = () => {
    if (auxAuthorization[1] === '<INSERT_VALID_ACCESS_TOKEN>') {
        console.log('Set up a valid token');
        return false;
    }
    return true;
}


const verify = (URL_SERVER_VALIDATOR) => {

    const validatorForm = {
        'nonce': certificateConfig.NONCE,
        'mode': certificateConfig.MODE,
        'contentReturn': certificateConfig.CONTENT_RETURN,
        'extensionsReturn': certificateConfig.EXTENSIONS_RETURN,
        'certificates[0][nonce]': certificateConfig.NONCE_OF_CERTIFICATE,
        'certificates[0][content]': fs.createReadStream(certificateConfig.CERTIFICATE_PATH)
    }

    return new Promise((resolve, reject) => {

        request.post({ url: URL_SERVER_VALIDATOR, formData: validatorForm, headers: this.headers }, (error, response, body) => {
            if (error) {
                reject(error);
            } else {
                resolve(JSON.parse(body));
            }
        })
    })

}


module.exports = { URL_SERVER_VALIDATOR, verify, configuredToken };