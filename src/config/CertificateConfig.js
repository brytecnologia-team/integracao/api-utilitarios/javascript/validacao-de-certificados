module.exports = {

    // Request identifier
    NONCE: 1,

    // Identifier of the certificate within a batch
    NONCE_OF_CERTIFICATE: 1,

    // Available values: 'CHAIN' = Report with certificate chain information and 'BASIC' = Report without the certificate chain information.
    MODE: 'CHAIN',

    // Available values: 'true' = Report with certificate content in Base64 and 'false' = Report without certificate content in Base64
    CONTENT_RETURN: 'true',

    // Available values: 'true' = Report with extension information, such as: authority key identifier, CRL distribution points, birth data and 'false' = Report without extension information
    EXTENSIONS_RETURN: 'true',

    // location where the certificate is stored
    CERTIFICATE_PATH: './certificado/cert.cer',
}